package ru.aliya.betboom;

import okhttp3.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static ru.aliya.betboom.UrlUtils.findEvenPages;
import static ru.aliya.betboom.UrlUtils.incrementPage;

/**
 * Created on 17.01.2023
 *
 * @author Aliya Kuznetsova (aliyakuzn@gmail.com)
 */
public class UrlUtilsTest {
    /**
     * Задача 2
     * Написать тест, который будет проверять корректность работы данной функции. Учесть, что к ссылке могут добавляться query параметры. Входные данные должны обрабатываться одним методом.
     */
    @Test
    public void testIncrementPage() {
        assertThrows(NullPointerException.class, () -> incrementPage(null, 1));

        assertUrlIsInvalid("");
        assertUrlIsInvalid(" ");
        assertUrlIsInvalid("bla");
        assertUrlIsInvalid("https://7/");
        assertUrlIsInvalid("123://lubart-miniatures.com/shop/page/7/");
        assertUrlIsInvalid("HTTPS://lubart-miniatures.com/shop/page/7/");
        assertUrlIsInvalid("https//lubart-miniatures.com/shop/page/7/");
        assertUrlIsInvalid("https://lubart-miniatures.com/shop/page//");
        assertUrlIsInvalid("https://lubart-miniatures.com/shop/page/7color=blue&sort=desc");

        assertEquals(
                "https://lubart-miniatures.com/shop/page/10/",
                incrementPage("https://lubart-miniatures.com/shop/page/7/", 3)
        );
        assertEquals(
                "https://lubart-miniatures.com/shop/page/100501/",
                incrementPage("https://lubart-miniatures.com/shop/page/100500/", 1)
        );
        assertEquals(
                "https://lubart-miniatures.com/shop/page/4/",
                incrementPage("https://lubart-miniatures.com/shop/page/7/", -3)
        );
        assertEquals(
                "https://lubart-miniatures.com/shop/page/7/",
                incrementPage("https://lubart-miniatures.com/shop/page/7/", 0)
        );
        assertEquals(
                "https://lubart-miniatures.com/22/page/10/",
                incrementPage("https://lubart-miniatures.com/22/page/7/", 3)
        );
        assertEquals(
                "https://333/22/page/10/",
                incrementPage("https://333/22/page/7/", 3)
        );
        assertEquals(
                "https:///22/page/10/",
                incrementPage("https:///22/page/7/", 3)
        );
        assertEquals(
                "https://lubart-miniatures.com/22//10/",
                incrementPage("https://lubart-miniatures.com/22//7/", 3)
        );
        assertEquals(
                "https://lubart-miniatures.com/shop/page/10?",
                incrementPage("https://lubart-miniatures.com/shop/page/7?", 3)
        );
        assertEquals(
                "https://lubart-miniatures.com/shop/page/10?color=blue",
                incrementPage("https://lubart-miniatures.com/shop/page/7?color=blue", 3)
        );
        assertEquals(
                "https://lubart-miniatures.com/shop/page/10?color=blue&sort=desc",
                incrementPage("https://lubart-miniatures.com/shop/page/7?color=blue&sort=desc", 3)
        );
        assertEquals(
                "https://lubart-miniatures.com/shop/page/10?22",
                incrementPage("https://lubart-miniatures.com/shop/page/7?22", 3)
        );
        assertEquals(
                "https://lubart-miniatures.com/shop/page/10?color=blue&sort=desc",
                incrementPage("https://lubart-miniatures.com/shop/page/7?color=blue&sort=desc", 3)
        );
        assertEquals(
                "http://lubart-miniatures.com/shop/page/10?color=blue&sort=desc",
                incrementPage("http://lubart-miniatures.com/shop/page/7?color=blue&sort=desc", 3)
        );
    }

    private static void assertUrlIsInvalid(String url) {
        assertThrows(
                IllegalArgumentException.class,
                () -> incrementPage(url, 1),
                () -> "Invalid url: " + url
        );
    }

    /**
     * Задача 3
     * Написать тест, который будет производить http запрос на этот url и проверять существование страницы.
     */
    @Test
    public void testHttpPageExists() throws IOException {
        final OkHttpClient client = new OkHttpClient();
        Call call = client.newCall(new Request.Builder()
                .url("https://lubart-miniatures.com/shop/page/7/")
                .build());
        Response response = call.execute();

        assertEquals(200, response.code());
        assertEquals("OK", response.message());

        ResponseBody body = response.body();
        assertNotNull(body);

        MediaType contentType = body.contentType();
        assertNotNull(contentType);
        assertEquals("text", contentType.type());
        assertEquals("html", contentType.subtype());

        String html = body.string();
        assertNotNull(html);
        assertTrue(html.startsWith("<!DOCTYPE html>"));

        //System.out.println(html);
    }

    /**
     * Задача 4
     * Написать функцию, которая на вход получает список ссылок подобного вида
     * и возвращает список только с четными значениями последнего ендпоинта
     */
    @Test
    public void testFindEvenPages() {
        assertEquals(
                List.of(
                        "https://lubart-miniatures.com/shop/page/0/",
                        "https://lubart-miniatures.com/shop/page/2/",
                        "https://lubart-miniatures.com/shop/page/4/",
                        "https://lubart-miniatures.com/shop/page/6/",
                        "https://lubart-miniatures.com/shop/page/8/"
                ),
                findEvenPages(List.of(
                        "https://lubart-miniatures.com/shop/page/0/",
                        "https://lubart-miniatures.com/shop/page/1/",
                        "https://lubart-miniatures.com/shop/page/2/",
                        "https://lubart-miniatures.com/shop/page/3/",
                        "https://lubart-miniatures.com/shop/page/4/",
                        "https://lubart-miniatures.com/shop/page/5/",
                        "https://lubart-miniatures.com/shop/page/6/",
                        "https://lubart-miniatures.com/shop/page/7/",
                        "https://lubart-miniatures.com/shop/page/8/",
                        "https://lubart-miniatures.com/shop/page/9/"
                ))
        );
    }
}
