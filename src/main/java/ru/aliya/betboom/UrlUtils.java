package ru.aliya.betboom;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Created on 17.01.2023
 *
 * @author Aliya Kuznetsova (aliyakuzn@gmail.com)
 */
public class UrlUtils {
    /**
     * https://lubart-miniatures.com/shop/page/7/
     * <схема>:[//[<логин>[:<пароль>]@]<хост>[:<порт>]][/<URL‐путь>][?<параметры>][#<якорь>]
     */
    private static final Pattern URL_WITH_PAGE_PATTERN =
            Pattern.compile("^https?://[-a-zA-Z0-9/.]*/(?<page>\\d+)(/?|\\?[-a-zA-Z0-9=&]*)$");

    /**
     * Задача 1
     * <p>
     * Есть ссылка вида:
     * https://lubart-miniatures.com/shop/page/7/
     * <p>
     * Написать функцию, которая инкрементирует значение последнего ендпоинта
     * на N-ое кол-во и вовзвращает полученный результат
     *
     * @param url   ссылка
     * @param count число, на которое нужно инкрементировать страницу
     * @return ссылка с инкрементированной страницей
     */
    public static String incrementPage(String url, int count) {
        requireNonNull(url);

        Matcher matcher = URL_WITH_PAGE_PATTERN.matcher(url);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Invalid url: " + url);
        }

        int pageStart = matcher.start("page");
        if (pageStart < 0) {
            throw new IllegalArgumentException("Invalid url: " + url);
        }

        int pageEnd = matcher.end("page");
        if (pageEnd <= 0) {
            throw new IllegalArgumentException("Invalid url: " + url);
        }

        int pageNum = Integer.parseInt(url.substring(pageStart, pageEnd));
        String prefix = url.substring(0, pageStart);
        String postfix = url.substring(pageEnd);

        return prefix + (pageNum + count) + postfix;
    }

    public static int extractPage(String url) {
        requireNonNull(url);

        Matcher matcher = URL_WITH_PAGE_PATTERN.matcher(url);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Invalid url: " + url);
        }

        return Integer.parseInt(matcher.group("page"));
    }

    /**
     * Задача 4
     * <p>
     * Написать функцию, которая на вход получает список ссылок подобного вида, и возвращает список только с четными значениями последнего ендпоинта
     *
     * @param urls - список ссылок
     * @return список ссылок с четными значениями последнего ендпоинта
     */
    public static List<String> findEvenPages(List<String> urls) {
        requireNonNull(urls);

        return urls.stream()
                .filter(s -> extractPage(s) % 2 == 0)
                .collect(Collectors.toList());
    }
}
